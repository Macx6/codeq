<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectStoreRequest;
use App\Jobs\CreateProjectRepository;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ProjectController extends Controller
{
    public function __construct() {
        $this->middleware('auth:sanctum');
        $this->authorizeResource(Project::class, 'project');
    }

    /**
     * Display a listing of the resource.
     *
     * @return array|\Illuminate\Http\Response
     */
    public function index()
    {
        return Auth::user()->allProjects()->take(5)->all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\ProjectStoreRequest $request
     *
     * @return false|\Illuminate\Database\Eloquent\Model|\Illuminate\Http\Response
     */
    public function store(ProjectStoreRequest $request)
    {
        $data = $request->validated();
        $data['image_src'] = $request->avatar->store('images');
        dispatch(new CreateProjectRepository());
        return Auth::user()->ownerships()->create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Project $project
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Project $project)
    {
        $project->delete();
    }
}
