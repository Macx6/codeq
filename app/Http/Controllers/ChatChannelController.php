<?php

namespace App\Http\Controllers;

use App\Models\ChatChannel;
use Illuminate\Http\Request;

class ChatChannelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ChatChannel  $chatChannel
     * @return \Illuminate\Http\Response
     */
    public function show(ChatChannel $chatChannel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ChatChannel  $chatChannel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChatChannel $chatChannel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ChatChannel  $chatChannel
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChatChannel $chatChannel)
    {
        //
    }
}
