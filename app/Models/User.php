<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;

/**
 * App\Models\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[]
 *                $notifications
 * @property-read int|null
 *                    $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[]
 *                    $tokens
 * @property-read int|null
 *                    $tokens_count
 * @method static Builder|\App\models\User newModelQuery()
 * @method static Builder|\App\models\User newQuery()
 * @method static Builder|\App\models\User query()
 * @mixin \Eloquent
 * @property int
 *               $id
 * @property string
 *               $name
 * @property string
 *               $email
 * @property \Illuminate\Support\Carbon|null
 *               $email_verified_at
 * @property string
 *               $password
 * @property string|null
 *               $remember_token
 * @property \Illuminate\Support\Carbon|null
 *               $created_at
 * @property \Illuminate\Support\Carbon|null
 *               $updated_at
 * @property int|null
 *               $current_project_id
 * @property string|null
 *               $profile_photo_path
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Project[]
 *                    $projects
 * @property-read int|null
 *                    $projects_count
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereCurrentProjectId($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereProfilePhotoPath($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Project[]
 *                    $ownerships
 * @property-read int|null
 *                    $ownerships_count
 * @property string|null $avatar
 * @property string $acronym
 * @method static Builder|User whereAcronym($value)
 * @method static Builder|User whereAvatar($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Issue[] $issues
 * @property-read int|null $issues_count
 */
class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'acronym',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['acronym'] = Str::of($value)->upper()
                                                      ->explode(' ')
                                                      ->map(fn($v) => substr($v, 0, 1))
                                                      ->join('');
    }

    /**
     * Get all of the teams the user belongs to.
     */
    public function projects()
    {
        return $this->belongsToMany(Project::class, Membership::class)
                    ->withTimestamps();
    }

    public function ownerships()
    {
        return $this->hasMany(Project::class, 'owner_id', 'id');
    }

    public function issues()
    {
        return $this->hasMany(Issue::class);
    }

    /**
     * Return all user projects with issues
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function allProjects()
    {
        return $this->ownerships()->with('issues')->get()->merge($this->projects()->with('issues')->get())->sortBy('updated_at');
    }
}
