<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ChatChannel
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ChatChannel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatChannel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatChannel query()
 * @mixin \Eloquent
 */
class ChatChannel extends Model
{
    use HasFactory;
}
