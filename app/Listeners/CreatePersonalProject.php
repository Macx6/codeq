<?php

namespace App\Listeners;

use App\Models\Project;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreatePersonalProject
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        if($event->user instanceof User) {
            $event->user->ownerships()->save(new Project([
                'name' => 'Personal',
                'description' => 'This is your personal project! You can add here your personal tasks.'
            ]));
        }
    }
}
