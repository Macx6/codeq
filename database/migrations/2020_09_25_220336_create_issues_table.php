<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issues', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->longText('description')->nullable();
            $table->foreignId('user_id')->nullable();
            $table->foreignId('assigned_user_id')->nullable();
            $table->foreignId('project_id')->nullable();
            $table->timestamp('start_date')->nullable();
            $table->timestamp('due_date')->nullable();
            $table->enum('priority', ['LOW', 'NORMAL', 'HIGH', 'IMMEDIATE'])->default('NORMAL');
            $table->enum('status', ['NEW', 'OPEN', 'CLOSED'])->default('NEW');
            $table->integer('finished')->default(0);
            $table->float('estimated_time')->default(0);
            $table->float('worked_time')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issues');
    }
}
