<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projects')->inserT([
            [
                'name'        => 'Personal',
                'description' => 'Personal project',
                'owner_id'    => 2,
                'slug'        => 'personal',
            ],
            [
                'name'        => 'First project',
                'description' => 'My first project',
                'owner_id'    => 2,
                'slug'        => 'first-project',
            ],
        ]);
    }
}
