<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IssueTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('issues')->insert([
            [
                'name'             => 'My first issue',
                'description'      => 'You can add more issues',
                'user_id'          => 2,
                'assigned_user_id' => 2,
                'project_id'       => 1,
                'start_date'       => Carbon::now(),
                'due_date'         => Carbon::now()->addDays(7),
            ],
            [
                'name'             => 'This is just an example',
                'description'      => 'Check out more related functions!',
                'user_id'          => 2,
                'assigned_user_id' => 2,
                'project_id'       => 1,
                'start_date'       => Carbon::now(),
                'due_date'         => Carbon::now()->addDays(7),
            ],
            [
                'name'             => 'Dashboard shows all your issues',
                'description'      => 'Here you can see all issues you are assigned',
                'user_id'          => 2,
                'assigned_user_id' => 2,
                'project_id'       => 2,
                'start_date'       => Carbon::now(),
                'due_date'         => Carbon::now()->addDays(7),
            ],
        ]);
    }
}
