<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'     => 'Admin Admin',
            'email'    => 'admin@admin.com',
            'password' => Hash::make('password'),
            'acronym'  => 'AA',
        ]);

        DB::table('users')->insert([
            'name'     => 'User1',
            'email'    => 'example@example.com',
            'password' => Hash::make('password'),
            'acronym'  => 'U',
        ]);
    }
}
