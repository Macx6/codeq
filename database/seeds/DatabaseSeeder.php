<?php

use Database\Seeders\IssueTableSeeder;
use Database\Seeders\ProjectTableSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(ProjectTableSeeder::class);
        $this->call(IssueTableSeeder::class);
    }
}
