import auth from './store/auth'
import app from './store/app'
import project from './store/project'
import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth: auth,
    app: app,
    project: project
  }
})
