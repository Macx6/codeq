export default {
  namespaced: true,
  state: {
    darkTheme: JSON.parse(localStorage.getItem('dark_theme')),
    snackbarMessage: '',
    bottomSheetOpen: false,
    loading: false
  },

  mutations: {
    setAppTheme (state, instance) {
      state.darkTheme = !state.darkTheme
      instance.theme.dark = state.darkTheme
      localStorage.setItem('dark_theme', state.darkTheme)
    },
    setSnackbar (state, message) {
      state.snackbarMessage = message
    },
    setBottomSheet (state, value) {
      state.bottomSheetOpen = value
    },
    setLoading (state, value) {
      state.loading = value
    }
  },

  actions: {},

  getters: {
  }

}
