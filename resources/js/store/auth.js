import axios from 'axios'

export default {
  namespaced: true,
  state: {
    user: null
  },

  mutations: {
    setUserData (state, userData) {
      state.user = userData
      localStorage.setItem('user', JSON.stringify(userData))
      axios.defaults.headers.common.Authorization = `Bearer ${userData.token}`
    },

    clearUserData () {
      localStorage.clear()
      location.reload()
    }
  },

  actions: {
    login ({ commit }, credentials) {
      return axios.post('/login', credentials).then(({ data }) => {
        commit('setUserData', data)
      })
    },
    register ({ commit }, credentials) {
      return axios.post('/register', credentials).then(({ data }) => {
        commit('setUserData', data)
      })
    },

    logout ({ commit }) {
      commit('clearUserData')
    }
  },

  getters: {
    isLogged: state => !!state.user
  }
}
