import axios from 'axios'
export default {
  namespaced: true,
  state: {
    currentProjectId: null,
    projects: []
  },
  mutations: {
    setProject (state, project) {
      if (project === null) {
        project = localStorage.getItem('last_active_project_id') !== null ? JSON.parse(localStorage.getItem('last_active_project_id')) : state.projects[state.projects.length - 1]
      }
      state.currentProjectId = project.id
      localStorage.setItem('last_active_project_id', project.id)
    },
    setProjects (state, projects) {
      Object.values(projects).map((project) => { project.image = 'storage/' + project.image_src })
      Object.values(projects).map((project) => { project.initials = project.name.split(' ').map(i => i.charAt(0)).join('') })

      state.projects = projects
    },
    deleteProject (state, project) {
      delete state.projects.find(item => item.id === project)
      localStorage.removeItem('last_active_project_id')
      this.setProject(state, null)
    }
  },

  actions: {
    getProjects ({ commit }) {
      return axios.get('/projects').then(results => {
        commit('setProjects', results.data)
      })
    },
    getProject ({ commit }, project) {
      return axios.get(`/projects/${project.id}`).then(result => {
        console.log(result)
      })
    },
    createProject ({ commit, dispatch }, project) {
      return axios.post('/projects', project).then(result => {
        dispatch('getProjects')
        commit('setProject', result.data)
      })
    },
    updateProject ({ commit, state }, project) {
      return axios.put(`/projects/${project.id}`, project)
    },
    deleteProject ({ commit, state }, project) {
      return axios.delete(`/projects/${project.id}`).then(
        commit('deleteProject', project)
      )
    }
  },

  getters: {
    project: state => state.projects.find(item => item.id === state.currentProjectId),
    issues: state => state.projects.map(item => item.issues).flat()
  }

}
