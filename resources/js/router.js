import VueRouter from 'vue-router'
import Vue from 'vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('./pages/Home.vue'),
    meta: {
      index: true,
      group: ''
    }
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: () => import('./pages/Dashboard'),
    meta: {
      auth: true,
      group: '',
      exclude: true,
      icon: 'mdi-view-dashboard'
    }
  },
  {
    path: '/analyser',
    name: 'Analyse',
    component: () => import('./pages/Code'),
    meta: {
      auth: true,
      group: 'code',
      icon: 'mdi-timeline-check-outline'
    }
  },
  {
    path: '/repository',
    name: 'Repository',
    component: () => import('./pages/Code'),
    meta: {
      auth: true,
      group: 'code',
      icon: 'mdi-file-document-multiple-outline'
    }
  },
  {
    path: '/project',
    name: 'Project overview',
    component: () => import('./pages/Project'),
    meta: {
      auth: true,
      group: 'project',
      icon: 'mdi-home'
    }
  },
  {
    path: '/new-project',
    name: 'New project',
    component: () => import('./pages/NewProject'),
    meta: {
      auth: true,
      exclude: true
    }
  },
  {
    path: '/issues',
    name: 'Issues',
    component: () => import('./pages/Issues'),
    meta: {
      auth: true,
      group: 'project',
      icon: 'mdi-format-list-checks'
    }
  },
  {
    path: '/team',
    name: 'Members',
    component: () => import('./pages/Members'),
    meta: {
      auth: true,
      group: 'project',
      icon: 'mdi-account-group'
    }
  },

  {
    path: '/chat',
    name: 'Chat',
    component: () => import('./pages/Members'),
    meta: {
      auth: true,
      group: 'chat',
      icon: 'mdi-forum'
    }
  },
  {
    path: '/profile',
    name: 'Profile',
    component: () => import('./pages/Profile'),
    meta: {
      auth: true,
      group: 'settings',
      exclude: true,
      icon: 'mdi-account'
    }
  }
]
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const loggedIn = localStorage.getItem('user')

  if (to.matched.some((record) => record.meta.auth) && !loggedIn) {
    next('/')
    return
  }
  next()
})
export default router
